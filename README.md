# Sedna CSS

A Free (as in Freedom), class-less, lightweight CSS framework!

## Features

- **Free as in Freedom!**
- **Class-less**: You do not need to use classes; Only pure html!
- **Bidi support**: Work for both RTL and LTR pages!
- **Lightweight**: It is less than 10KiB!
- **Responsice**: Looks good in small screens!
- **Dark & Light mode**: Automaticly dark mode with optional force-light and force-dark modes!
- **Palettes**: You can *optionaly* use classes to use your favourite color palette!
- **Colors**: You can *optionaly* use classes to coloring parts!
- **Fonts**: You can *optionaly* use classes to change font!

## How to use

1. Put sedna.css in `css` folder, near your html file.
2. An example sedna page:
```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Base informations -->
        <meta charset="utf-8">
        <title>Title</title>
        <meta name="description" content="description">

        <!-- Mobile specific metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Font -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/rastikerdar/vazirmatn@v33.003/Vazirmatn-font-face.css">

        <!-- CSS -->
        <link rel="stylesheet" href="css/sedna.css">

        <!-- Favicon -->
        <link rel="icon" type="image/png" href="images/favicon.png">

    </head>
    <body>
        <header>
            <nav>
                <a href="#">nav 1</a>
                <a href="#">nav 2</a>
            </nav>
            <h1>Title</h1>
            <p>description</p>
        </header>
        <main>
            /* your content is here */
        </main>
    </body>
</html>
```

3. Set your favourite palette:
```html
<html class="palette">
...
</html>
```

- **Palettes**: reasonable (default), adwaita, nord, dracula, solarized, catppuccin, gruvbox, tango

4. Set your favourite font:
```html
<html class="font">
...
</html>
```

- **Fonts**: [vazirmatn](https://github.com/rastikerdar/vazirmatn) (default), [sahel](https://github.com/rastikerdar/sahel-font), [shabnam](https://github.com/rastikerdar/shabnam-font), [samim](https://github.com/rastikerdar/samim-font), [gandom](https://github.com/rastikerdar/gandom-font), [tanha](https://github.com/rastikerdar/tanha-font), [parastoo](https://github.com/rastikerdar/parastoo-font), [nahid](https://github.com/rastikerdar/nahid-font)

you should load the font you want!


4. force-dark or force-light mode:
```html
<html class="force-dark">
...
</html>
```

6. Use colors everywhere you like:
```html
<p class="color">text</p>
```

- **Colors**: red, green, yellow, blue, purple

## License

[![GNU GPL v3](./LICENSE.png)](./LICENSE)
